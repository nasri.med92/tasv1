<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TasController extends AbstractController
{
    /**
     * @Route("/tas", name="tas_index")
     */
    public function index(): Response
    {
        return new Response('Bienvenue Module TAS');
    }
}